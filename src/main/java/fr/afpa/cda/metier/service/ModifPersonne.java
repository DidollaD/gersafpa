package fr.afpa.cda.metier.service;

import fr.afpa.cda.dto.service.ModifPersonneDto;
import fr.afpa.cda.metier.entity.Authentification;
import fr.afpa.cda.metier.entity.Personne;

public class ModifPersonne {
	public void CreerUser(Personne p, Authentification auth) {
		ModifPersonneDto spd = new ModifPersonneDto();
		spd.CreerUser(p, auth);
	}

	public boolean EnregistrerAu(String login, String mdp) {
		ModifPersonneDto mpdt = new ModifPersonneDto();
		Authentification auth = mpdt.EnregistrerAuthentif(login, mdp);
		if(auth==null) {
			return false;
		}
		return true;

	}

	public void DeleteUser(Personne p, Authentification auth) {
		ModifPersonneDto sdp = new ModifPersonneDto();
		sdp.DeleteUser(p, auth);
	}


	public void ModifierPersonne (Personne p, Authentification auth){
		ModifPersonneDto mpd = new ModifPersonneDto();
		mpd.ModifierUser(p, auth);
		
}
}
