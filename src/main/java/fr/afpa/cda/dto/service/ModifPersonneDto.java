package fr.afpa.cda.dto.service;

import java.util.ArrayList;

import fr.afpa.cda.dao.entity.AuthentificationDao;
import fr.afpa.cda.dao.entity.PersonneDao;
import fr.afpa.cda.dao.service.ModifPersonneDao;
import fr.afpa.cda.metier.entity.Authentification;
import fr.afpa.cda.metier.entity.Personne;

public class ModifPersonneDto {

	public void CreerUser(Personne p, Authentification auth) {
		ModifPersonneDao mp = new ModifPersonneDao();
		PersonneDao perDao = new PersonneDao();
		AuthentificationDao authDao = new AuthentificationDao();
		authDao.setLogin(auth.getLogin());
		authDao.setMdp(auth.getMdp());
		authDao.setAdmin(auth.isAdmin());
		perDao.setNom(p.getNom());
		perDao.setPrenom(p.getPrenom());
		perDao.setDateNais(p.getDateNais());
		perDao.setNumAdr(p.getNumAdr());
		perDao.setNomAdr(p.getNomAdr());
		perDao.setVille(p.getVille());
		perDao.setMail(p.getMail());
		perDao.setTel(p.getTel());
		authDao.setPerso(perDao);
		perDao.setAuth(authDao);
		mp.CreerUser(perDao, authDao);
	}

	public Authentification EnregistrerAuthentif(String login, String mdp) {
		ModifPersonneDao mpd = new ModifPersonneDao();
		AuthentificationDao authDao = mpd.Enregistrement(login, mdp);
		Authentification auth = new Authentification();
		if (authDao != null) {
			auth.setLogin(authDao.getLogin());
			auth.setMdp(authDao.getMdp());
			auth.setAdmin(authDao.isAdmin());
			return auth;
		}
		return null;

	}

	public void DeleteUser(Personne p, Authentification auth) {
		ModifPersonneDao pm = new ModifPersonneDao();
		PersonneDao perDao = new PersonneDao();
		AuthentificationDao authDao = new AuthentificationDao();
		authDao.setLogin(auth.getLogin());
		authDao.setMdp(auth.getMdp());
		authDao.setAdmin(auth.isAdmin());
		perDao.setNom(p.getNom());
		perDao.setPrenom(p.getPrenom());
		perDao.setDateNais(p.getDateNais());
		perDao.setNumAdr(p.getNumAdr());
		perDao.setNomAdr(p.getNomAdr());
		perDao.setVille(p.getVille());
		perDao.setMail(p.getMail());
		perDao.setTel(p.getTel());
		authDao.setPerso(perDao);
		perDao.setAuth(authDao);
		pm.DeleteUser(perDao, authDao);

	}

	public ArrayList<Authentification> listerPersonne() {
		ModifPersonneDao mpd = new ModifPersonneDao();
		ArrayList<AuthentificationDao> listeDao = mpd.listerAuthentification();
		ArrayList<Authentification> liste = new ArrayList<Authentification>();

		for (AuthentificationDao authDao : listeDao) {
			Personne pers = new Personne();
			Authentification auth = new Authentification();
			pers.setNom(authDao.getPerso().getNom());
			pers.setPrenom(authDao.getPerso().getPrenom());

		}
		return liste;
	}
	public void ModifierUser (Personne p, Authentification auth) {
		ModifPersonneDao mp = new ModifPersonneDao();
		PersonneDao perDao = new PersonneDao();
		AuthentificationDao authDao = new AuthentificationDao();
		authDao.setLogin(auth.getLogin());
		authDao.setMdp(auth.getMdp());
		authDao.setAdmin(auth.isAdmin());
		perDao.setNom(p.getNom());
		perDao.setPrenom(p.getPrenom());
		perDao.setDateNais(p.getDateNais());
		perDao.setNumAdr(p.getNumAdr());
		perDao.setNomAdr(p.getNomAdr());
		perDao.setVille(p.getVille());
		perDao.setMail(p.getMail());
		perDao.setTel(p.getTel());
		authDao.setPerso(perDao);
		perDao.setAuth(authDao);
		mp.ModifierUser(perDao, authDao);
	}
	
}

