package fr.afpa.cda.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.cda.control.ControlAuthentification;
import fr.afpa.cda.metier.entity.Authentification;
import fr.afpa.cda.metier.service.ModifPersonne;

/**
 * Servlet implementation class Validation
 */
public class Validation extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Validation() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModifPersonne mp = new ModifPersonne();
		String login = request.getParameter("login");
		String pwd = request.getParameter("password");
		
		if(mp.EnregistrerAu(login, pwd)) {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/Menu.jsp");
            dispatcher.forward(request, response);
		}
		
		

		
		//		ControlAuthentification ca = new ControlAuthentification();
//		response.setContentType("text/html");
//		PrintWriter out = response.getWriter();
//		if (ca.ValideAuth(request.getParameter("login"), request.getParameter("password"))) {
//			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/Menu.jsp");
//            dispatcher.forward(request, response);
//		}
	}

}
