package fr.afpa.cda.servlet;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.cda.metier.entity.Authentification;
import fr.afpa.cda.metier.entity.Personne;
import fr.afpa.cda.metier.service.ModifPersonne;

/**
 * Servlet implementation class Creation
 */
public class Creation extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Creation() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModifPersonne us = new ModifPersonne();

		Date sqlDate = new Date(0);
		try {
			sqlDate.setTime((new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("dateNais")).getTime()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Personne p = new Personne(request.getParameter("nom"), request.getParameter("prenom"), sqlDate,
				Integer.parseInt(request.getParameter("numRue")), request.getParameter("rue"),
				request.getParameter("ville"), request.getParameter("mail"), request.getParameter("tel"));
		Authentification auth = new Authentification(request.getParameter("login"), request.getParameter("password"),
				Boolean.parseBoolean(request.getParameter("admin")));
		us.CreerUser(p,auth);
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/Menu.jsp");
		dispatcher.forward(request, response);
	}

}
