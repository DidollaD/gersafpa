package fr.afpa.cda.control;

import java.util.ArrayList;

import org.hibernate.Query;
import org.hibernate.Session;

import fr.afpa.cda.dao.entity.AuthentificationDao;
import fr.afpa.cda.utils.HibernateUtils;

public class ControlAuthentification {

	public boolean ValideAuth(String login, String mdp) {
		
		Session s = HibernateUtils.getSession();
		Query q = s.createQuery("from AuthentificationDao");
		ArrayList<AuthentificationDao> list = (ArrayList<AuthentificationDao>) q.list();
		for (AuthentificationDao authentification : list) {
			if (login.equals(authentification.getLogin()) && mdp.equals(authentification.getMdp())) {
				s.close();
				return true;
			}
		}
		s.close();
		return false;
	}
}
