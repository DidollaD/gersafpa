package fr.afpa.cda.dao.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "personne")
public class PersonneDao {
	@Column
	@Id
	@GeneratedValue
	private int idPers;
	@Column
	private String nom;
	@Column
	private String prenom;
	@Column
	private Date dateNais;
	@Column
	private int numAdr;
	@Column
	private String nomAdr;
	@Column
	private String ville;
	@Column
	private String mail;
	@Column
	private String tel;
	@OneToOne(mappedBy = "perso")
	private AuthentificationDao auth;
	
	
	
	/**
	 * 
	 */
	public PersonneDao() {
		super();
	}



	/**
	 * @param nom
	 * @param prenom
	 * @param dateNais
	 * @param numAdr
	 * @param nomAdr
	 * @param ville
	 * @param mail
	 * @param tel
	 * @param auth
	 */
	public PersonneDao(String nom, String prenom, Date dateNais, int numAdr, String nomAdr, String ville, String mail,
			String tel) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateNais = dateNais;
		this.numAdr = numAdr;
		this.nomAdr = nomAdr;
		this.ville = ville;
		this.mail = mail;
		this.tel = tel;
	}



	/**
	 * @return the idPers
	 */
	public int getIdPers() {
		return idPers;
	}



	/**
	 * @param idPers the idPers to set
	 */
	public void setIdPers(int idPers) {
		this.idPers = idPers;
	}



	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}



	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}



	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}



	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}



	/**
	 * @return the dateNais
	 */
	public Date getDateNais() {
		return dateNais;
	}



	/**
	 * @param dateNais the dateNais to set
	 */
	public void setDateNais(Date dateNais) {
		this.dateNais = dateNais;
	}



	/**
	 * @return the numAdr
	 */
	public int getNumAdr() {
		return numAdr;
	}



	/**
	 * @param numAdr the numAdr to set
	 */
	public void setNumAdr(int numAdr) {
		this.numAdr = numAdr;
	}



	/**
	 * @return the nomAdr
	 */
	public String getNomAdr() {
		return nomAdr;
	}



	/**
	 * @param nomAdr the nomAdr to set
	 */
	public void setNomAdr(String nomAdr) {
		this.nomAdr = nomAdr;
	}



	/**
	 * @return the ville
	 */
	public String getVille() {
		return ville;
	}



	/**
	 * @param ville the ville to set
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}



	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}



	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}



	/**
	 * @return the tel
	 */
	public String getTel() {
		return tel;
	}



	/**
	 * @param tel the tel to set
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}



	/**
	 * @return the auth
	 */
	public AuthentificationDao getAuth() {
		return auth;
	}



	/**
	 * @param auth the auth to set
	 */
	public void setAuth(AuthentificationDao auth) {
		this.auth = auth;
	}
	
	
}
