package fr.afpa.cda.dao.service;

import java.util.ArrayList;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import fr.afpa.cda.dao.entity.AuthentificationDao;
import fr.afpa.cda.dao.entity.PersonneDao;
import fr.afpa.cda.utils.HibernateUtils;

public class ModifPersonneDao {
	
	public void CreerUser(PersonneDao perDao, AuthentificationDao authDao) {
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		s.save(perDao);
		s.save(authDao);
		tx.commit();
		s.close();
	}
	
	
	public ArrayList<AuthentificationDao> listerAuthentification(){
		Session s = HibernateUtils.getSession();
		Query q = s.createQuery("from Authentification");
		
		ArrayList<AuthentificationDao> list = (ArrayList<AuthentificationDao>) q.list();
		
		return list;
		
	}
	public AuthentificationDao Enregistrement(String login, String mdp) {
		Session s = HibernateUtils.getSession();
		Query q=s.createQuery("from AuthentificationDao where login='"+login+"' and mdp='"+mdp+"'");
		
		ArrayList<AuthentificationDao> cptlist = (ArrayList<AuthentificationDao>) q.list();

		
		s.close();
		return cptlist.get(0);
		
	}
	
	public void DeleteUser(PersonneDao perDao, AuthentificationDao authDao) {
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		s.delete(perDao);
		s.delete(authDao);
		tx.commit();
		s.close();
		
	}
	public void ModifierUser(PersonneDao perDao, AuthentificationDao authDao) {
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		s.save(perDao);
		s.save(authDao);
		tx.commit();
		s.close();
	}
	
}
